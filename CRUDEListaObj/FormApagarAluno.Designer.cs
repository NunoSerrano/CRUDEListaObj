﻿namespace CRUDEListaObj
{
    partial class FormApagarAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridViewAlunoApagar = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonApagar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewAlunoApagar)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridViewAlunoApagar
            // 
            this.DataGridViewAlunoApagar.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridViewAlunoApagar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewAlunoApagar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.DataGridViewAlunoApagar.Location = new System.Drawing.Point(120, 32);
            this.DataGridViewAlunoApagar.Name = "DataGridViewAlunoApagar";
            this.DataGridViewAlunoApagar.Size = new System.Drawing.Size(465, 150);
            this.DataGridViewAlunoApagar.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Id Aluno";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "PrimeiroNome";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Apelido";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // ButtonApagar
            // 
            this.ButtonApagar.Image = global::CRUDEListaObj.Properties.Resources.if_button_ok_1710;
            this.ButtonApagar.Location = new System.Drawing.Point(355, 250);
            this.ButtonApagar.Name = "ButtonApagar";
            this.ButtonApagar.Size = new System.Drawing.Size(93, 65);
            this.ButtonApagar.TabIndex = 1;
            this.ButtonApagar.UseVisualStyleBackColor = true;
            this.ButtonApagar.Click += new System.EventHandler(this.ButtonApagar_Click);
            // 
            // button1
            // 
            this.button1.Image = global::CRUDEListaObj.Properties.Resources.if_Delete_1493279;
            this.button1.Location = new System.Drawing.Point(492, 250);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(93, 65);
            this.button1.TabIndex = 2;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormApagarAluno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 357);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ButtonApagar);
            this.Controls.Add(this.DataGridViewAlunoApagar);
            this.Name = "FormApagarAluno";
            this.Text = "ApagarAluno";
            this.Load += new System.EventHandler(this.FormApagarAluno_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewAlunoApagar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewAlunoApagar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Button ButtonApagar;
        private System.Windows.Forms.Button button1;
    }
}