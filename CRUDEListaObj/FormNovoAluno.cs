﻿
namespace CRUDEListaObj


{

    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using Modelos;// É necessário colocar este using porque o alunos está dentro de Modelos
    public partial class FormNovoAluno : Form
    {
        private List<Aluno> ListaDeAlunos = new List<Aluno>(); // não está a criar uma nova lista mas vai a pontar para o outro lado

        public FormNovoAluno(List<Aluno>ListaDeAlunos) // construtor
        {
            InitializeComponent();
            this.ListaDeAlunos = ListaDeAlunos;// Importante, é uma referência
        }

        private void ButtonNovoAluno_Click(object sender, EventArgs e)
        {

            // VALIDAÇÕES
        if (string.IsNullOrEmpty(TextBoxPrimeiroNome.Text))
        {
            MessageBox.Show("tem que inserir o primeiro nome do aluno.");
            return;
        }

        if (string.IsNullOrEmpty(TextBoxApelido.Text))
        {
            MessageBox.Show("tem que inserir o apelido do aluno.");
            return;
        }

        // IMPORTANTE, não é necessário so construtores porque ele vai buscar o mesmo 
            var aluno = new Aluno
            {
                IdAluno = GerarIdAluno(),
                PrimeiroNome = TextBoxPrimeiroNome.Text,
                Apelido = TextBoxApelido.Text,
            };


            ListaDeAlunos.Add(aluno);// apontador para a lista do outro lado
            MessageBox.Show("Novo aluno inserido com sucesso");
            Close();

        }


        private int GerarIdAluno()
        {
            return ListaDeAlunos[ListaDeAlunos.Count - 1].IdAluno + 10;             
            // vai buscar o ultimo da lista no ID aluno e soma mais 10;
            // retorna para o ID Aluno
        }
    }
}
