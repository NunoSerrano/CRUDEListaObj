﻿namespace CRUDEListaObj
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ComboBoxNomes = new System.Windows.Forms.ComboBox();
            this.ComboBoxListaAlunos = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ButtonProcurar = new System.Windows.Forms.Button();
            this.TextBoxProcurar = new System.Windows.Forms.TextBox();
            this.ComboBoxProcurar = new System.Windows.Forms.ComboBox();
            this.GroupBoxEditar = new System.Windows.Forms.GroupBox();
            this.TextBoxApelidoAluno = new System.Windows.Forms.TextBox();
            this.TextBoxNomeAluno = new System.Windows.Forms.TextBox();
            this.TextBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBoxIDAluno = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.novoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.apagarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.actualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ButtonGravar = new System.Windows.Forms.Button();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.ButtonEditar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.GroupBoxEditar.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ComboBoxNomes);
            this.groupBox1.Controls.Add(this.ComboBoxListaAlunos);
            this.groupBox1.Location = new System.Drawing.Point(391, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(270, 138);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // ComboBoxNomes
            // 
            this.ComboBoxNomes.FormattingEnabled = true;
            this.ComboBoxNomes.Location = new System.Drawing.Point(12, 38);
            this.ComboBoxNomes.Name = "ComboBoxNomes";
            this.ComboBoxNomes.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxNomes.TabIndex = 1;
            // 
            // ComboBoxListaAlunos
            // 
            this.ComboBoxListaAlunos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxListaAlunos.FormattingEnabled = true;
            this.ComboBoxListaAlunos.Location = new System.Drawing.Point(12, 81);
            this.ComboBoxListaAlunos.Name = "ComboBoxListaAlunos";
            this.ComboBoxListaAlunos.Size = new System.Drawing.Size(225, 21);
            this.ComboBoxListaAlunos.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButtonProcurar);
            this.groupBox2.Controls.Add(this.TextBoxProcurar);
            this.groupBox2.Controls.Add(this.ComboBoxProcurar);
            this.groupBox2.Location = new System.Drawing.Point(12, 70);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(351, 126);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Procurar";
            // 
            // ButtonProcurar
            // 
            this.ButtonProcurar.Location = new System.Drawing.Point(139, 69);
            this.ButtonProcurar.Name = "ButtonProcurar";
            this.ButtonProcurar.Size = new System.Drawing.Size(75, 23);
            this.ButtonProcurar.TabIndex = 3;
            this.ButtonProcurar.Text = "Procurar";
            this.ButtonProcurar.UseVisualStyleBackColor = true;
            this.ButtonProcurar.Click += new System.EventHandler(this.ButtonProcurar_Click);
            // 
            // TextBoxProcurar
            // 
            this.TextBoxProcurar.Location = new System.Drawing.Point(176, 27);
            this.TextBoxProcurar.Name = "TextBoxProcurar";
            this.TextBoxProcurar.Size = new System.Drawing.Size(169, 20);
            this.TextBoxProcurar.TabIndex = 2;
            // 
            // ComboBoxProcurar
            // 
            this.ComboBoxProcurar.FormattingEnabled = true;
            this.ComboBoxProcurar.Items.AddRange(new object[] {
            "Id Aluno",
            "Nome"});
            this.ComboBoxProcurar.Location = new System.Drawing.Point(28, 26);
            this.ComboBoxProcurar.Name = "ComboBoxProcurar";
            this.ComboBoxProcurar.Size = new System.Drawing.Size(116, 21);
            this.ComboBoxProcurar.TabIndex = 0;
            // 
            // GroupBoxEditar
            // 
            this.GroupBoxEditar.Controls.Add(this.ButtonGravar);
            this.GroupBoxEditar.Controls.Add(this.ButtonCancelar);
            this.GroupBoxEditar.Controls.Add(this.ButtonEditar);
            this.GroupBoxEditar.Controls.Add(this.TextBoxApelidoAluno);
            this.GroupBoxEditar.Controls.Add(this.TextBoxNomeAluno);
            this.GroupBoxEditar.Controls.Add(this.TextBoxPrimeiroNome);
            this.GroupBoxEditar.Controls.Add(this.label4);
            this.GroupBoxEditar.Controls.Add(this.label3);
            this.GroupBoxEditar.Controls.Add(this.label2);
            this.GroupBoxEditar.Controls.Add(this.TextBoxIDAluno);
            this.GroupBoxEditar.Controls.Add(this.label1);
            this.GroupBoxEditar.Location = new System.Drawing.Point(12, 195);
            this.GroupBoxEditar.Name = "GroupBoxEditar";
            this.GroupBoxEditar.Size = new System.Drawing.Size(792, 136);
            this.GroupBoxEditar.TabIndex = 2;
            this.GroupBoxEditar.TabStop = false;
            this.GroupBoxEditar.Text = "Editar";
            // 
            // TextBoxApelidoAluno
            // 
            this.TextBoxApelidoAluno.Enabled = false;
            this.TextBoxApelidoAluno.Location = new System.Drawing.Point(379, 92);
            this.TextBoxApelidoAluno.Name = "TextBoxApelidoAluno";
            this.TextBoxApelidoAluno.Size = new System.Drawing.Size(170, 20);
            this.TextBoxApelidoAluno.TabIndex = 9;
            // 
            // TextBoxNomeAluno
            // 
            this.TextBoxNomeAluno.Location = new System.Drawing.Point(379, 32);
            this.TextBoxNomeAluno.Name = "TextBoxNomeAluno";
            this.TextBoxNomeAluno.ReadOnly = true;
            this.TextBoxNomeAluno.Size = new System.Drawing.Size(170, 20);
            this.TextBoxNomeAluno.TabIndex = 8;
            // 
            // TextBoxPrimeiroNome
            // 
            this.TextBoxPrimeiroNome.Enabled = false;
            this.TextBoxPrimeiroNome.Location = new System.Drawing.Point(88, 95);
            this.TextBoxPrimeiroNome.Name = "TextBoxPrimeiroNome";
            this.TextBoxPrimeiroNome.Size = new System.Drawing.Size(182, 20);
            this.TextBoxPrimeiroNome.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nome:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(299, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Apelido";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(299, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Nome Aluno:";
            // 
            // TextBoxIDAluno
            // 
            this.TextBoxIDAluno.Location = new System.Drawing.Point(88, 32);
            this.TextBoxIDAluno.Name = "TextBoxIDAluno";
            this.TextBoxIDAluno.ReadOnly = true;
            this.TextBoxIDAluno.Size = new System.Drawing.Size(111, 20);
            this.TextBoxIDAluno.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "ID Aluno:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoToolStripMenuItem,
            this.actualizarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(829, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // novoToolStripMenuItem
            // 
            this.novoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.apagarToolStripMenuItem,
            this.novoToolStripMenuItem1});
            this.novoToolStripMenuItem.Name = "novoToolStripMenuItem";
            this.novoToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.novoToolStripMenuItem.Text = "Aluno";
            // 
            // apagarToolStripMenuItem
            // 
            this.apagarToolStripMenuItem.Name = "apagarToolStripMenuItem";
            this.apagarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.apagarToolStripMenuItem.Text = "Apagar";
            this.apagarToolStripMenuItem.Click += new System.EventHandler(this.apagarToolStripMenuItem_Click);
            // 
            // novoToolStripMenuItem1
            // 
            this.novoToolStripMenuItem1.Name = "novoToolStripMenuItem1";
            this.novoToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.novoToolStripMenuItem1.Text = "Novo";
            this.novoToolStripMenuItem1.Click += new System.EventHandler(this.novoToolStripMenuItem1_Click);
            // 
            // actualizarToolStripMenuItem
            // 
            this.actualizarToolStripMenuItem.Name = "actualizarToolStripMenuItem";
            this.actualizarToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.actualizarToolStripMenuItem.Text = "Actualizar";
            this.actualizarToolStripMenuItem.Click += new System.EventHandler(this.actualizarToolStripMenuItem_Click);
            // 
            // ButtonGravar
            // 
            this.ButtonGravar.Enabled = false;
            this.ButtonGravar.Image = global::CRUDEListaObj.Properties.Resources.if_button_ok_1710;
            this.ButtonGravar.Location = new System.Drawing.Point(588, 85);
            this.ButtonGravar.Name = "ButtonGravar";
            this.ButtonGravar.Size = new System.Drawing.Size(75, 30);
            this.ButtonGravar.TabIndex = 12;
            this.ButtonGravar.UseVisualStyleBackColor = true;
            this.ButtonGravar.Click += new System.EventHandler(this.ButtonGravar_Click);
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.Enabled = false;
            this.ButtonCancelar.Image = global::CRUDEListaObj.Properties.Resources.if_Delete_1493279;
            this.ButtonCancelar.Location = new System.Drawing.Point(669, 85);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(75, 30);
            this.ButtonCancelar.TabIndex = 11;
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            this.ButtonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // ButtonEditar
            // 
            this.ButtonEditar.Image = global::CRUDEListaObj.Properties.Resources.if_081_Pen_183209;
            this.ButtonEditar.Location = new System.Drawing.Point(588, 19);
            this.ButtonEditar.Name = "ButtonEditar";
            this.ButtonEditar.Size = new System.Drawing.Size(156, 46);
            this.ButtonEditar.TabIndex = 10;
            this.ButtonEditar.UseVisualStyleBackColor = true;
            this.ButtonEditar.Click += new System.EventHandler(this.ButtonEditar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 390);
            this.Controls.Add(this.GroupBoxEditar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
           
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.GroupBoxEditar.ResumeLayout(false);
            this.GroupBoxEditar.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ComboBoxListaAlunos;
        private System.Windows.Forms.ComboBox ComboBoxNomes;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox ComboBoxProcurar;
        private System.Windows.Forms.Button ButtonProcurar;
        private System.Windows.Forms.TextBox TextBoxProcurar;
        private System.Windows.Forms.GroupBox GroupBoxEditar;
        private System.Windows.Forms.Button ButtonGravar;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.Button ButtonEditar;
        private System.Windows.Forms.TextBox TextBoxApelidoAluno;
        private System.Windows.Forms.TextBox TextBoxNomeAluno;
        private System.Windows.Forms.TextBox TextBoxPrimeiroNome;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TextBoxIDAluno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem apagarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novoToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem actualizarToolStripMenuItem;
    }
}

