﻿namespace CRUDEListaObj
{
    partial class FormNovoAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxIDAluno = new System.Windows.Forms.TextBox();
            this.TextBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.TextBoxApelido = new System.Windows.Forms.TextBox();
            this.ButtonNovoAluno = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Aluno:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Primeiro Nome";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apelido:";
            // 
            // TextBoxIDAluno
            // 
            this.TextBoxIDAluno.Location = new System.Drawing.Point(139, 34);
            this.TextBoxIDAluno.Name = "TextBoxIDAluno";
            this.TextBoxIDAluno.ReadOnly = true;
            this.TextBoxIDAluno.Size = new System.Drawing.Size(100, 20);
            this.TextBoxIDAluno.TabIndex = 3;
            // 
            // TextBoxPrimeiroNome
            // 
            this.TextBoxPrimeiroNome.Location = new System.Drawing.Point(139, 88);
            this.TextBoxPrimeiroNome.Name = "TextBoxPrimeiroNome";
            this.TextBoxPrimeiroNome.Size = new System.Drawing.Size(100, 20);
            this.TextBoxPrimeiroNome.TabIndex = 4;
            // 
            // TextBoxApelido
            // 
            this.TextBoxApelido.Location = new System.Drawing.Point(139, 161);
            this.TextBoxApelido.Name = "TextBoxApelido";
            this.TextBoxApelido.Size = new System.Drawing.Size(100, 20);
            this.TextBoxApelido.TabIndex = 5;
            // 
            // ButtonNovoAluno
            // 
            this.ButtonNovoAluno.Image = global::CRUDEListaObj.Properties.Resources.if_button_ok_1710;
            this.ButtonNovoAluno.Location = new System.Drawing.Point(319, 131);
            this.ButtonNovoAluno.Name = "ButtonNovoAluno";
            this.ButtonNovoAluno.Size = new System.Drawing.Size(75, 53);
            this.ButtonNovoAluno.TabIndex = 6;
            this.ButtonNovoAluno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButtonNovoAluno.UseVisualStyleBackColor = true;
            this.ButtonNovoAluno.Click += new System.EventHandler(this.ButtonNovoAluno_Click);
            // 
            // FormNovoAluno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 218);
            this.Controls.Add(this.ButtonNovoAluno);
            this.Controls.Add(this.TextBoxApelido);
            this.Controls.Add(this.TextBoxPrimeiroNome);
            this.Controls.Add(this.TextBoxIDAluno);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormNovoAluno";
            this.Text = "Novo Aluno";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxIDAluno;
        private System.Windows.Forms.TextBox TextBoxPrimeiroNome;
        private System.Windows.Forms.TextBox TextBoxApelido;
        private System.Windows.Forms.Button ButtonNovoAluno;
    }
}