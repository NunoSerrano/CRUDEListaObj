﻿
namespace CRUDEListaObj
{
    using System.Windows.Forms;
    using Modelos;
    using System.IO;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;


    public partial class Form1 : Form
    {

        private List<Aluno> ListaDeAlunos; // é preciso instanciar
                                           // ListaDeAlunos é também um ojecto


        private FormNovoAluno formularioNovoAluno;

        private FormApagarAluno formularioApagarAluno;



        public Form1()
        {
            InitializeComponent();

            ListaDeAlunos = new List<Aluno>();

            if (!CarregarAlunos()) // Se não carregou vai carregar, se carregou passa ao seguinte
            {
                ListaDeAlunos.Add(new Aluno { IdAluno = 16343, PrimeiroNome = "Amadeu", Apelido = "Antunes" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16328, PrimeiroNome = "António", Apelido = "Almeida" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 14990, PrimeiroNome = "Daniel", Apelido = "Veiga" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16342, PrimeiroNome = "João", Apelido = "Ribeiro" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16343, PrimeiroNome = "Luis", Apelido = "Gomes" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16343, PrimeiroNome = "Hugo", Apelido = "Pereira" });
                ListaDeAlunos.Add(new Aluno { IdAluno = 16334, PrimeiroNome = "Nuno", Apelido = "Serrano" });

            }



            ComboBoxListaAlunos.DataSource = ListaDeAlunos;

            foreach (var aluno in ListaDeAlunos)
            {

                ComboBoxNomes.Items.Add(aluno.PrimeiroNome);
            }

           
        }

        private void ButtonProcurar_Click(object sender, EventArgs e)
        {
            if (ComboBoxProcurar.SelectedIndex == -1)
            {
                MessageBox.Show("Tem que escolher um critério");
                return;
            }

            int id = 0;

            if (ComboBoxProcurar.SelectedIndex == 0)
            {
                if (!int.TryParse(TextBoxProcurar.Text, out id))
                {
                    MessageBox.Show("Tem que escolher um valor numérico");
                    return;//sai da função em que está
                }

                var alunoAchado = ProcurarAluno(id);

                if (alunoAchado != null)
                {

                    MessageBox.Show(alunoAchado.NomeCompleto);
                    TextBoxIDAluno.Text = alunoAchado.IdAluno.ToString();
                    TextBoxNomeAluno.Text = alunoAchado.NomeCompleto;
                }
                else
                {
                    MessageBox.Show("Esse is e aluno não existe");
                }

            }

            else
            {

                var alunoAchado = ProcuraAluno(TextBoxProcurar.Text);

                if (alunoAchado != null)
                {

                    MessageBox.Show(alunoAchado.NomeCompleto);
                    TextBoxIDAluno.Text = alunoAchado.IdAluno.ToString();
                    TextBoxNomeAluno.Text = alunoAchado.NomeCompleto;
                }
                else
                {
                    MessageBox.Show("Esse is e aluno não existe");
                }
            }


        }

        private Aluno ProcuraAluno(string nome)
        {
            foreach (var aluno in ListaDeAlunos)
            {
                if (aluno.PrimeiroNome == nome)
                {
                    return aluno;
                }
            }

            return null;
        }

        private Aluno ProcurarAluno(int id)
        {
            foreach (var aluno in ListaDeAlunos)
            {
                if (aluno.IdAluno == id)
                {
                    return aluno;
                }
            }

            return null;
        }

        private void ButtonEditar_Click(object sender, EventArgs e)
        {
            TextBoxPrimeiroNome.Enabled = true;
            TextBoxApelidoAluno.Enabled = true;
            ButtonGravar.Enabled = true;
            ButtonCancelar.Enabled = true;


        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            TextBoxPrimeiroNome.Enabled = false;
            TextBoxApelidoAluno.Enabled = false;
            ButtonGravar.Enabled = false;
            ButtonCancelar.Enabled = false;

        }

        private void ButtonGravar_Click(object sender, EventArgs e)
        {

            // se as ciaxas estiverem empty envia mensagem
            if (string.IsNullOrEmpty(TextBoxIDAluno.Text))
            {
                MessageBox.Show("Insira id de aluno");
                return;
            }

            if (string.IsNullOrEmpty(TextBoxPrimeiroNome.Text))
            {
                MessageBox.Show("Insira o primeiro nome do aluno");
            }

            if (string.IsNullOrEmpty(TextBoxApelidoAluno.Text))
            {
                MessageBox.Show("Insira o apelido do aluno");
            }


            // posso ter um atributo de nome ou vo ubuscar a lista novamente

            var alunoAchado = ProcurarAluno(Convert.ToInt32(TextBoxIDAluno.Text));// é o aluno que está na lista

            if (alunoAchado != null)
            {
                alunoAchado.PrimeiroNome = TextBoxPrimeiroNome.Text;
                alunoAchado.Apelido = TextBoxApelidoAluno.Text;
                MessageBox.Show("Aluno alterado com sucesso!");

                actualizaCombos();

                TextBoxPrimeiroNome.Text = String.Empty;
                TextBoxPrimeiroNome.Enabled = true;

                TextBoxApelidoAluno.Text = String.Empty;
                TextBoxApelidoAluno.Enabled = false;

                ButtonGravar.Enabled = false;
                ButtonCancelar.Enabled = false;
            }


        }


        private void actualizaCombos()
        {

            ComboBoxListaAlunos.DataSource = null;
            ComboBoxListaAlunos.DataSource = ListaDeAlunos;

            ComboBoxNomes.Items.Clear();


            foreach (var aluno in ListaDeAlunos)
            {
                ComboBoxNomes.Items.Add(aluno.PrimeiroNome);
            }


        }

        private bool GravarAlunos()
        {
            string ficheiro = @"alunos.txt";

            StreamWriter sw = new StreamWriter(ficheiro, false);

            try// detectar erros (estudar o try catch e mensagem de erro)
            {
                if (!File.Exists(ficheiro))
                {
                    sw = File.CreateText(ficheiro);// quando vai ver se existe pode ahver problemas bem como quando grava etc
                }

                // ir a Lista 1 a 1

                foreach (var aluno in ListaDeAlunos)
                {
                    string linha = string.Format("{0};{1};{2}", aluno.IdAluno, aluno.PrimeiroNome, aluno.Apelido);
                    sw.WriteLine(linha);
                }

                sw.Close();

            }
            catch (Exception e)

            {
                MessageBox.Show(e.Message);
                return false;
            }


            return true; // para confirmar que gravou
        }






        private bool CarregarAlunos()
        {
            string ficheiro = @"Alunos.txt";
            StreamReader sr;

            try
            {
                if (File.Exists(ficheiro))
                {
                    sr = File.OpenText(ficheiro);

                    string linha = "";


                    while ((linha = sr.ReadLine()) != null)
                    {
                        string[] campos = new string[3];
                        campos = linha.Split(';');


                        var aluno = new Aluno
                        // objecto aluno criado de novo para gravar dentro da lista
                        // cria um objecto novo em cada while epor cada aluno
                        {
                            IdAluno = Convert.ToInt32(campos[0]),
                            PrimeiroNome = campos[1],
                            Apelido = campos[2]
                        };


                        ListaDeAlunos.Add(aluno);
                    }

                    sr.Close();
                }
                else
                {
                    return false;
                }
            }




            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
            return true;
        }


        //Fazer para quando passamos para baixo o nome fique já nas caixas de texto e depois só alteramos o que queremos.



        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        // no form ir a events e depois duplo clicar em CLosed para aparecer este método
        {

            if (GravarAlunos())
            {
                MessageBox.Show("Alunos gravados com sucesso");
            }


        }

        private void novoToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            formularioNovoAluno = new FormNovoAluno(ListaDeAlunos);//Cria o novo formulário e envia a lista para esse formulário
            formularioNovoAluno.Show();


        }

        private void actualizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            actualizaCombos();
            MessageBox.Show("alunos actualizados com sucesso.");
        }

        private void apagarToolStripMenuItem_Click(object sender, EventArgs e)
        {



            if (string.IsNullOrEmpty(TextBoxIDAluno.Text))
            {
                MessageBox.Show("Insira id a procurar");
                return;
            }


            var alunoApagado = ProcurarAluno(Convert.ToInt32(TextBoxIDAluno.Text));

 

           

            formularioApagarAluno.Show();
        }
        
    }

    }

