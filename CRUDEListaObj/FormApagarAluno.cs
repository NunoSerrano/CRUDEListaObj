﻿

namespace CRUDEListaObj
{
    using Modelos;
    using System.Collections.Generic;
    using System.Windows.Forms;
    public partial class FormApagarAluno : Form
    {
        private List<Aluno> ListaDeAlunos;

        private Aluno alunoAApagar;

        private Form1 formPrincipal;
        public FormApagarAluno(List<Aluno> ListaAlunos, Aluno aluno, Form1 formPrincipal)
        {
            InitializeComponent();


            this.formPrincipal = formPrincipal;

            this.ListaDeAlunos = ListaAlunos;
            alunoAApagar = aluno;

            DataGridViewAlunoApagar.Rows.Add(
                alunoAApagar.IdAluno, 
                alunoAApagar.PrimeiroNome, 
                alunoAApagar.Apelido);

            DataGridViewAlunoApagar.AllowUserToAddRows = false;
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("O aluno não foi apagado.");
            Close(); // geralemnet poem-se this.Close(), mas na pratica e por defeito faz o this
        }

        private void ButtonApagar_Click(object sender, System.EventArgs e)
        {

            // remover o aluno e actiualiza o index da lista 
            int index = 0;

            foreach (var aluno in ListaDeAlunos)
            {
                if(alunoAApagar.IdAluno == aluno.IdAluno)
                {
                    ListaDeAlunos.RemoveAt(index); // remove o aluno
                    break;
                }

                index++;
            }

            

            MessageBox.Show("Aluno apagado com sucesso");

            Close();

        }

        private void FormApagarAluno_Load(object sender, System.EventArgs e)
        {

        }
    }
}
