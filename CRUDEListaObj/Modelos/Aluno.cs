﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDEListaObj.Modelos
{
    public class Aluno
    {

        public int IdAluno { get; set; }
        public string PrimeiroNome { get; set; }

        public string Apelido { get; set; }

        public string NomeCompleto
        {
            get { return string.Format("{0} {1}", PrimeiroNome, Apelido); }

        }



        public override string ToString()
        {
            return string.Format("IdAluno:{0} Nome:{1}", IdAluno, NomeCompleto);
        }


      


    }
}
